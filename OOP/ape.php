<?php

require_once 'Animal.php';

class Ape extends Animal
{
    public function yell()
    {
        echo "Auooo";
    }

    // Override property legs to 2
    public $legs = 2;
}
