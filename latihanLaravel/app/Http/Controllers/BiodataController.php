<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class BiodataController extends Controller
{
    public function signup() {
        return view('pages.form');
    }

    public function register(Request $request)
{
    $first_name = $request->input('first_name');
    $last_name = $request->input('last_name');

    return view('pages.home')->with(['first_name' => $first_name, 'last_name' => $last_name]);
}

}
