<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<body>
    <form action="/signup" method="post">
        @csrf
        <label for="first_name">First Name:</label><br>
        <input type="text" id="first_name" name="first_name"><br><br>
     
        <label for="last_name">Last Name:</label><br>
        <input type="text" id="last_name" name="last_name"><br><br>
     
        <label>Gender:</label><br>
        <input type="radio" id="male" name="gender" value="male">
        <label for="male">Male</label><br>
        <input type="radio" id="female" name="gender" value="female">
        <label for="female">Female</label><br>
        <input type="radio" id="other" name="gender" value="other">
        <label for="other">Other</label><br><br>
     
        <label for="nationality">Nationality:</label><br>
        <select id="nationality" name="nationality">
           <option value="Indonesia">Indonesia</option>
           <option value="UK">UK</option>
           <option value="Canada">Canada</option>
           <option value="Australia">Australia</option>
           
        </select><br><br>
     
        <label>Language Spoken:</label><br>
        <input type="checkbox" id="indo" name="language_spoken" value="indo">
        <label for="english">Bahasa Indonesia</label><br>
        <input type="checkbox" id="inggris" name="language_spoken" value="inggris">
        <label for="spanish">Bahasa Inggris</label><br>
        <input type="checkbox" id="other" name="language_spoken" value="other">
        <label for="french">Other</label><br>
        
     
        <br><br>
     
        <label for="bio">Bio:</label><br>
        <textarea id="bio" name="bio" rows="4" cols="50"></textarea><br><br>
     
        <button type="submit">Submit</button>
     </form>
     
</body>
</html>
